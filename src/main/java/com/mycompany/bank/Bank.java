/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.bank;

import java.util.Scanner;

/**
 *
 * @author extre
 */
public class Bank {
    
    private static Double getValor() {
        System.out.println("Digite o valor: ");
        Scanner valor = new Scanner(System.in);
        return valor.nextDouble();
    }

    private static double investimento(double valor) {
        double aumento = valor*0.05;
        return aumento;
    }

    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double saldo = 0;
        String opcao = "";
        while(!opcao.equals("0")){
            System.out.println("Bem-vindo Jones Radtke ao HomeBank SENAC!");
            System.out.println("#########################################");
            System.out.println("# 1. Ver extrato da conta               #");
            System.out.println("# 2. Depósito em conta                  #");
            System.out.println("# 3. Saque da conta                     #");
            System.out.println("# 4. pix                                #");
            System.out.println("# 5. pagamento                          #");
            System.out.println("# 6. investimento                       #");
            System.out.println("# 0. Sair                               #");
            System.out.println("#########################################");
            System.out.println("Digite uma das opções acima:");
            Scanner tc = new Scanner(System.in);
            opcao = tc.next();

            switch (opcao) {
                case "1":
                    System.out.println("Saldo:.....R$: " + saldo);
                    break;
                case "2":
                    saldo = saldo + getValor();
                    System.out.println("Saldo:.....R$: " + saldo);
                    break;
                case "3":
                    saldo = saldo - getValor();
                    System.out.println("Saldo:.....R$: " + saldo);
                    break;
                case "4":
                System.out.print("\nInforme o CPF do destinatário: ");
                Scanner scannerCpf = new Scanner(System.in);
                String cpfDestino = scannerCpf.next();
                System.out.println("Qual valor deseja enviar? ");
                Double valorPix = getValor();
                if(saldo >= valorPix){
                    System.out.println("Transferência realizada com sucesso.");
                    saldo -= valorPix;
                } else {
                    System.out.println("Saldo insuficiente para transferência.");
                }
                break;
                case "6":
                    System.out.print("Entre com o valor que deseja investir (aumento de 5%): \nR$");
                    double invest = input.nextDouble();
                    double aumento = investimento(invest);
                    saldo = saldo + aumento; 
                    System.out.println("Saldo com o investimento:.....R$: " + saldo);
                    break;
                case "0":
                    System.out.println("Saindo da aplicação...");
                    break;
                default:
                    System.out.println("Opção inválida!");
            }
        }
    }
}
